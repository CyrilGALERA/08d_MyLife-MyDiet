// MODEL - DATA --------------------------------------------------
// Get var + eventListener
//
let pwdInputList = document.getElementsByClassName('pwd');
for (let pwdInput of pwdInputList) {
    pwdInput.addEventListener('click', checkPwd);
}

// CONTROLLER ----------------------------------------------------- 
// Event listener functions
//
function checkPwd(pwdUser, pwdForm, choix0, choix1, choix2) {
    if( document.getElementById(choix1).innerText == 'Annuler') {
        // Return to initial state choix0 == choix Init == 'Supprimer' ou 'Modifier'
        document.getElementById(choix1).innerText=choix0;
        document.getElementById(pwdForm).value = ""
        document.getElementById(choix2).style.visibility = "hidden";
    } else { 
        // Check the password 
        pwd1 = document.getElementById(pwdUser).value;
        pwd2 = document.getElementById(pwdForm).value;
        console.log(pwd1, pwd2);

        if( pwd1 == pwd2 ) {
            // Display Confirmer-Annuler
            alert("Attention : veuillez annuler ou confirmer votre choix");
            document.getElementById(choix2).style.visibility="visible";
            document.getElementById(choix1).innerText="Annuler";
        } else {
            // Display alert invalid pwd
            alert("Mot de passe invalide, veuillez recommencer");
        }
        return true;
    }
}

function checkPwdSuppr() {
    checkPwd('pwd-suppr', 'pwd-form', 'Supprimer', 'suppr-choix1', 'suppr-choix2');
}
function checkPwdModif() {
    checkPwd('pwd-modif', 'pwd-form21', 'Modifier', 'modif-choix1', 'modif-choix2');
}

// function checkPwd(event) {
//     pwdUser = event.target.getAttribute("pwd-user");
//     pwdForm = event.target.value;

//     if( pwdUser == pwdForm ) {
//         // Display Confirmer-Annuler
//         if (document.getElementById("suppr-choix").innerText=="Supprimer") {
//             document.getElementById("suppr-cpt").style.visibility="visible";
//             document.getElementById("suppr-choix").innerText="Annuler";
//         } else {
//             // if Annuler : return back first choice Supprimer
//             document.getElementById("suppr-cpt").style.visibility = "hidden";
//             document.getElementById("suppr-choix").innerText="Supprimer";
//         }
//     } else {
//         // Display alert invalid pwd
//         alert("Mot de passe invalide, veuillez recommencer");
//     }
// }

// VUE : Display ---------------------------
//

// TMP - old code to remove ===========================================
//
// <SCRIPT>
// // <BUTTON TYPE="button" ONCLICK="confirmFunction()">Click me!</BUTTON>
// // <input  id="suppr-choix" type="button" onclick="myFunction2()" value="Supprimer"> 

// // < ?php  echo '<script>myfunction()< /script>'; 

// function myFunction() {
// alert("I am an alert box!"); //this is the message in ""
// }
// function myFunction2() {
//     alert("I am an alert box!"); //this is the message in ""
// }
// function confirmFunction() {
// if (confirm("Confirmer la suppression du compte?") == true) {
//     document.getElementById("ColorChanger").style.backgroundColor = "green";
// } else {
//     document.getElementById("ColorChanger").style.backgroundColor = "#C0C0C0";
// }
// }
// function confirmFunction2() {
// if (document.getElementById("suppr-choix").innerText=="Supprimer") {
//     document.getElementById("suppr-cpt").style.visibility="visible";
//     document.getElementById("suppr-choix").innerText="Annuler";
// } else {
//     document.getElementById("suppr-cpt").style.visibility = "hidden";
//     document.getElementById("suppr-choix").innerText="Supprimer";
// }
// }
// </SCRIPT>