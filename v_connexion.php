<?php
    // connexion var and POST SeConnecter et S-inscrire
    $toConnect = NULL;
    $connectionResult = NULL;

    session_start();

    if( !$_SESSION['connectMsg'] ) {} else {
        $connectionResult = $_SESSION['connectMsg'];
    } 
    // echo '<br> toConnect = '.$toConnect.'<br>';
    // echo '<br> connectionResult = '.$connectionResult.'<br>';
    
    // include 'debug.php';
    include 'a_config.php';
    // PDO to connect to DB
    include './DB_SQL/m_data_func.php';
    // Check form if SeConnecter or S-inscrire
    include 'c_connexion.php';

    if($toConnect == "Connecté") {
        $toConnect = NULL;
        $connectionResult = "Etes vous sur de vouloir vous déconnecter ? toConnect = ".$toConnect;
        // Display Page Web autre fichier v_deconnexion.php...
        include 'v_deconnexion.php';
    } else {
        // Display Page Web : connexion ci dessous ...

// HTML page ----------------------------------
  include 'v0_header.php';
?>

  <main>

    <?php if($connectionResult == NULL) {
        $connectionResult = 'Veuillez vous connecter ou vous inscrire pour utiliser l appli My Life - My Diet';
    } ?>

    <div class="margin-V5">
        <!-- <h3 class="t4-cyan box1 center-txt"><?php echo $_SESSION['connectMsg'];?></h3> -->
        <h3 class="t4-cyan box1 center-txt"><?php echo $connectionResult;?></h3>
        <!-- <h4><?php echo 'session = '.$_SESSION['password'];?></h4> -->
    </div>
    
    <div class="ligne axe1-sp-around">
        <div>
        <h2>Connexion</h2>
        <form class="col box1 dx350px" action="v_connexion.php" method="post">
        <!-- <form class="col box1 dx350px" action="c_connexion.php" method="post"> -->
            <div class="ligne axe1-sp-between">
                <label for="email">Identifiant : Email</label>
                <input type="text" name="email" placeholder="Rentrez votre email...">
            </div>
            <div class="ligne axe1-sp-between">
                <label for="pwd">Mot de passe</label>
                <input type="password" name="pwd" placeholder="Rentrez votre mot de passe...">
            </div>
            <div class="ligne axe1-center">
                <input type="submit" name="SeConnecter" value="Se Connecter">
            </div>
        </form>
        </div>

        <div>
        <h2>S'inscrire</h2>
        <form class="col box1 dx350px" action="v_connexion.php" method="post">
        <!-- <form class="col box1 dx350px" action="c_connexion.php" method="post"> -->
            <div class="ligne axe1-sp-between">
                <label for="email">Identifiant : Email</label>
                <input type="text" name="email" placeholder="Rentrez votre email...">
            </div>
            <div class="ligne axe1-sp-between">
                <label for="prenom">Prénom</label>
                <input type="text" name="prenom" placeholder="Rentrez votre Prénom..." >
            </div>
            <div class="ligne axe1-sp-between">
                <label for="nom">Nom</label>
                <input type="text" name="nom" placeholder="Rentrez votre Nom..." >
            </div>
            <div class="ligne axe1-sp-between">
                <label for="dateAnniv">Date de naissance</label>
                <input type="date" name="dateAnniv" >
            </div>
            <div class="ligne axe1-sp-between">
                <label for="pwd">Mot de passe</label>
                <input type="password" name="pwd" placeholder="Rentrez votre mot de passe...">
            </div>
            <div class="ligne axe1-center">
                <input type="submit" name="S-inscrire" value="S'inscrire">
            </div>
        </form>
        </div>
    </div>

  </main>
  <?php include 'v0_footer.php'; ?>
</body>
</html>

<?php } ?>