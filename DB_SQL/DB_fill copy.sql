-- Use script: sudo mysql < DB_fill.sql;

-- DATABASE:    MyLife_MyDiet;
-- TABLE:       Users, IMC, BesoinsCaloriques
--
-- -------------------------------------------------------------
-- Use script: sudo mysql < DB_init.sql;
--
-- sudo mysql
-- mysql> ...
-- show databases;
-- use MyLife_MyDiet;
-- show tables;
-- describe (table_name);
-- select * from (table_name);
-- quit;
-- exit;
-- -------------------------------------------------------------

USE MyLife_MyDiet;

-- TABLE Users -----------------------------

INSERT INTO Users (pseudo, password, nom, prenom, dateNaissance, sexe, taille)
  VALUES ("cyril", "cyril123", "Cyril", "Galera", "1975-12-18", 'masculin', 1,75),
         ("kevin", "kevin123", "Kevin", "Simplon", "2000-01-01", 'masculin', 1,78),
         ("bob", "bob123", "Bob", "Sinclair", "1980-08-25", 'masculin', 1,76),
         ("Marie", "mama123", "Marie", "De Narbonne", "1985-08-24", 1.60);

CREATE TABLE IMC (idUser, date, poids, IMC) 
  VALUES (1, "2022-08-21", 80.5, 27),
         (1, "2022-08-22", 80.5, 27),
         (1, "2022-08-23", 82.5, 28);

CREATE TABLE BesoinsCaloriques (idUser, date, poids, nivActivite, besoinsCal) 
  VALUES (1, "2022-08-21", 80.5, 1.3, 3000),
         (1, "2022-08-22", 80.5, 1.3, 3000),
         (1, "2022-08-23", 82.5, 1.2, 3250);