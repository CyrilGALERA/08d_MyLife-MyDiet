<?php
  
include 'c_connexion.php';
global $toConnect;
$toConnect = "Déconnexion";

// HTML page ----------------------------------
include './DB_SQL/m_data_func.php';
include 'v0_header.php';
session_start();
?>

  <main>

    <div class="margin-V5">
      <?php $user = getUser($_SESSION['email']); ?>

      <h3 class="t4-cyan box1 center-txt">Bonjour <?php echo $user['prenom'];?>... Etes vous sûr de vouloir vous déconnecter ? </h3>
      <!-- <h4 class="center-text"><?php echo $connectionMsg;?></h4> -->
      <!-- <h4><?php echo 'session = '.$_SESSION['password'];?></h4> -->
    </div>

      <div>
      <form action="v_connexion.php" method="post">
        <input class="Nav-btn-1" type="submit" name="SeDeconnecter" value="Se Déconnecter">
      </form>
      </div>
    </div>

  </main>
  <?php include 'v0_footer.php'; ?>
</body>
</html>