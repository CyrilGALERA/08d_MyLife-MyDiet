<?php     

// Connexion to the DB ===========================================
$host = 'localhost';
$bdd = 'MyLife_MyDiet';
$user = 'u_cyril';
$pwd = 'pwd_cyril';

$pdo = new PDO("mysql:host=$host;dbname=$bdd;", $user, $pwd);


// DATA - Users - CRUD ===============================================

// Users - CRUD Create -------------------------------
//
function createUser($email, $password, $nom, $prenom, $dateNaissance) {
    global $pdo;
    $stm = $pdo->prepare('INSERT INTO Users (email, password, nom, prenom, dateNaissance) VALUES (?,?,?,?,?);');
    echo '<br';
    var_dump($stm);
    $res = $stm->execute([$email, $password, $prenom, $nom, $dateNaissance]);
    echo '<br';

    var_dump($res);

    $user = $stm->fetch();
    echo '<br';

    var_dump($user);

    return $user;
}
// Users - CRUD Read -------------------------------
//
function getUser($email) {
    global $pdo;
    $stm = $pdo->prepare('SELECT * FROM Users WHERE email = ?;');
    $stm->execute([$email]);
    return $stm->fetch();
}

function getUserId($id) {
    global $pdo;
    $stm = $pdo->prepare('SELECT * FROM Users WHERE id = ?;');
    $stm->execute([$id]);
    return $stm->fetch();
}

// Juste une seule function getUser($colName, $colValue) {


// Users CRUD - Update -------------------------------
//
function updateUserData($id, $colName, $colValue)
{
    global $pdo;
    $stm = $pdo->prepare('UPDATE Users SET '.$colName.'=? WHERE id=?;');
    $stm->execute([$colValue, $id]);
    return getUserId($id);
}

// Users - CRUD Delete -------------------------------
//
function deleteUser($id) {
    // echo '<br> deleteUser res <br>';
    // $resultats = getResultats($id);
    // var_dump($resultats);
    // if( $resultats ) {
    //     foreach( $resultats as $res) {
    //         deleteResultat($res['id']);
    //         echo '<br> delete user res <br>';
    //     }
    // }
    echo '<br> deleteUser user <br>';

    global $pdo;
    $stm = $pdo->prepare('DELETE FROM Users WHERE id=?;');
    var_dump($stm);

    var_dump($stm->execute([$id]));

    return $stm->execute([$id]);
}


// DATA - Resultat - CRUD ===============================================

// Resultats - CRUD Create ----------------------
//
function createResultat($idUser, $today, $poids, $imc, $nivActivite, $besCal) {
    global $pdo;
    $stm = $pdo->prepare('INSERT INTO Resultats (idUser, date, poids, imc, nivActivite, besoinsCal) VALUES (?,?,?,?,?,?)');
    return $stm->execute([$idUser, $today, $poids, $imc, $nivActivite, $besCal]);
}

// Resultats - CRUD Read ----------------------
//
function getResultats($userId) {
    global $pdo;
    $stm = $pdo->prepare('SELECT * FROM Resultats WHERE idUser = ? ORDER BY date DESC;');
    $stm->execute([$userId]);
    return $stm->fetchAll();
}

function printTodayResultats($resultats) {
    $today = date('Y-m-d');
    $printResultat = [];
    if( $today > $resultats[0]['date'] ) {
        // Pas de données pour today
        $printResultat[0]="Votre IMC est de ...";
        $printResultat[1]="Vos besoins caloriques sont de ...";
    } else {
        $printResultat[0]="Votre IMC est de ".$resultats[0]['imc'];
        $printResultat[1]="Vos besoins caloriques sont de ".$resultats[0]['besoinsCal'];
    }
    return $printResultat;
}

// Resultats - CRUD Update ----------------------
//
function updateResultat($id, $idUser, $date, $poids, $imc, $nivActivite, $besCal) {
    global $pdo;
    $stm = $pdo->prepare('UPDATE Resultats SET idUser=?, date=?, poids=?, imc=?, nivActivite=?, besoinsCal=? WHERE id=?;');
    return $stm->execute([$idUser, $date, $poids, $imc, $nivActivite, $besCal, $id]);
}

// Resultats - CRUD Delete ----------------------
//
// function deleteResultat($id) {
//     global $pdo;
//     $stm = $pdo->prepare('DELETE FROM Resultats WHERE id=?;');
//     return $stm->execute([$id]);
// }

// NOT DATA table functions =============================================

// IMC - BesoinsCal ...........................................
//
// function computeColorIMC($imc) {
//     $styleCode = '<style> tr:nth-child(even) { background-color: #D6EEEE;} </style>';

//     $color = 
//     if($imc>0) {
//         if( $imc <18.5 ) {
//             $resultat = "maigre";
//         }else if( $imc <25 ) {
//             $resultat = "normal";
//         }else if( $imc <30 ) {
//             $resultat = "surpoids";
//         }else if( $imc <35 ) {
//             $resultat = "obésité modéré";
//         }else if( $imc <40 ) {
//             $resultat = "obésité sévère";
//         }else {
//             $resultat = "obésité morbide";
//         }
//     }
// }

function computeIMC($poids, $taille) {
    return round( ($poids/$taille/$taille), 1);
}
function computeBesCalFemme($poids, $taille, $age, $nivActivite) {
    return round( ((9.740 * $poids) + (172.9 * $taille) - (4.737 * $age) + 667.051) * $nivActivite);
}
function computeBesCalHomme($poids, $taille, $age, $nivActivite) {
    return round( ((13.707 * $poids) + (492.3 * $taille) - (6.673 * $age) + 77.607) * $nivActivite);
}
function computeBesCal($sexe, $poids, $taille, $age, $nivActivite) {
    $res = 0;
    if( $sexe == 'Homme') {
        $res = computeBesCalHomme($poids, $taille, $age, $nivActivite);
    } else {
        $res = computeBesCalFemme($poids, $taille, $age, $nivActivite);
    }
    return $res;
}
//
function corpulence($imc) {
    $resultat = "";
    if($imc>0) {
        if( $imc <18.5 ) {
            $resultat = "maigre";
        }else if( $imc <25 ) {
            $resultat = "normal";
        }else if( $imc <30 ) {
            $resultat = "surpoids";
        }else if( $imc <35 ) {
            $resultat = "obésité modéré";
        }else if( $imc <40 ) {
            $resultat = "obésité sévère";
        }else {
            $resultat = "obésité morbide";
        }
    }
    return $resultat;
}
function profilActivite($nivActivite) {
    $resultat = "";
    if($nivActivite>0) {
        if( $nivActivite <1.3 ) {
            $resultat = "(sédentaire)";
        } else if( $nivActivite <1.45 ) {
            $resultat = "(lég. actif)";
        } else if( $nivActivite <1.625 ) {
            $resultat = "(actif)";
        } else {
            $resultat = "(très actifs)";
        }
    }
    return $resultat;
}

// Date functions .............................................
//
// --- date()
// $today = date('Y-m-d');
// echo '<br> today = '.$today."<br>"; // today = 2022-08-25
//
// --- dateDifference() 
// return the diff in nb of days between the two date
// Ex : $age = dateDifference($today, $dateAnniv) / 365.25;
//
function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' ) {
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
    $interval = date_diff($datetime1, $datetime2);
    return $interval->format($differenceFormat);
}

// TMP ===============================================

    // echo "<br> getResultats - stm2 = <br>";
    // var_dump($stm);

    // $res = $stm->execute([$userId]);
    // echo "<br> getResultats - res = <br>";
    // var_dump($res);

    // $res2 = $stm->fetchAll();
    // echo "<br> getResultats - res2 = <br>";
    // var_dump($res2);  
    // return $res2;


// function getResultats($userId) {
//     global $pdo;
//     $stm = $pdo->prepare('SELECT * FROM Resultats WHERE idUser = ?;');
//     // $stm = $pdo->prepare('SELECT * FROM Resultats WHERE idUser = '.$userId.';');

//     echo "<br> getResultats - stm2 = <br>";
//     var_dump($stm);

//     $res = $stm->execute([$userId]);
//     // $res = $stm->execute();
//     echo "<br> getResultats - res = <br>";
//     var_dump($res);

//     // $res2 = $stm->fetch();
//     $res2 = $stm->fetchAll();
//     echo "<br> getResultats - res2 = <br>";
//     var_dump($res2);

//     // $stm->execute([$userId]);
//     return $res2;
// }

// function getRes() {
//     global $pdo;
//     $stm = $pdo->prepare('SELECT * FROM Resultats;');
//     $stm = $pdo->prepare('SELECT * FROM Resultats WHERE idUser = $userId;');
//                      //   select * from Resutalts where idUser = 1;
//     echo "<br> getResultats - stm2 = <br>";
//     var_dump($stm);

//     $res = $stm->execute();
//     echo "<br> getResultats - res = <br>";
//     var_dump($res);

//     $res2 = $stm->fetch();
//     echo "<br> getResultats - res2 = <br>";
//     var_dump($res2);

//     // $stm->execute([$userId]);
//     return $res2;
// }

?>