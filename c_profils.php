<?php
// session_start();
include './DB_SQL/m_data_func.php';

if(isset($_POST['Confirmer-suppr'])) {
    // echo 'Supprimer-compte <br>';
    // echo '<br> POST[Password] = '. $_POST['Password'].'<br>';
    // echo 'POST[idUser] <br>'. $_POST['idUser'];

    // $prenom = getUserId($_POST['idUser'])['prenom'];
    global $toConnect;
    global $connectionResult;
    $toConnect = NULL;
    $connectionResult = NULL;

    deleteUser($_POST['idUser']);
    // Il faut créer une fonction connexionType() = "connecté, déconnecté, supprimé..."
    $_SESSION['email'] = NULL;
    $_SESSION['pwd'] = NULL;
    $_SESSION['connectMsg'] = NULL;
    session_destroy();
    header('Location: v_connexion.php');
}

if(isset($_POST['Modif-email'])) {
    if( isset($_POST['email']) && $_POST['email'] !="" ) {
        updateUserData($_POST['idUser'], 'email', $_POST['email']);
        header('Location: v_profils.php#Cpt1');
    } else {
        header('Location: v_profils.php#Cpt1');
    }
}

if(isset($_POST['Modif-pwd'])) {
    if( isset($_POST['pwd22']) && $_POST['idUser'] !="" ) {
        updateUserData($_POST['idUser'], 'password', $_POST['pwd22']);
        header('Location: v_profils.php#Cpt1');
    } else {
        header('Location: v_profils.php#Cpt1');
    }
}

if(isset($_POST['Modif-prenom'])) {
    if( isset($_POST['prenom']) && $_POST['prenom'] !="" ) {
        updateUserData($_POST['idUser'], 'prenom', $_POST['prenom']);
        header('Location: v_profils.php#Cpt1');
    } else {
        header('Location: v_profils.php#Cpt1');
    }
}

if(isset($_POST['Modif-nom'])) {
    if( isset($_POST['nom']) && $_POST['nom'] !="") {
        updateUserData($_POST['idUser'], 'nom', $_POST['nom']);
        header('Location: v_profils.php#Cpt1');
    } else {
        header('Location: v_profils.php#Cpt1');
    }
}

if(isset($_POST['Modif-dateAnniv'])) {
    if( isset($_POST['dateAnniv']) ) {
        // echo '<br> POST[dateAnniv] = '.$_POST['dateAnniv'];
        updateUserData($_POST['idUser'], 'dateNaissance', $_POST['dateAnniv']);
        header('Location: v_profils.php#Cpt1');
    } else {
        // echo '<br> POST[dateAnniv] 22= ';
        header('Location: v_profils.php#Cpt1');
    }
}

if(isset($_POST['Modif-sexe'])) {
    if( isset($_POST['sexe']) && $_POST['sexe'] !="" ) {
        updateUserData($_POST['idUser'], 'sexe', $_POST['sexe']);
        header('Location: v_profils.php#Cpt1');
    } else {
        header('Location: v_profils.php#Cpt1');
    }
}

if(isset($_POST['Modif-taille'])) {
    if( isset($_POST['taille']) && $_POST['taille'] >0 ) {
        updateUserData($_POST['idUser'], 'taille', $_POST['taille']);
        header('Location: v_profils.php#Cpt1');
    } else {
        header('Location: v_profils.php#Cpt1');
    }
}

if(isset($_POST['calculer'])) {
    if( isset($_POST['poids']) && isset($_POST['nivActivite'])
    && $_POST['poids'] >0 && $_POST['nivActivite'] >0 ) {
        $user = getUserId($_POST['idUser']);
        $imc = computeIMC($_POST['poids'], $user['taille']);

        $today = date('Y-m-d');
        $age = dateDifference($today, $user['dateNaissance']) / 365.25;
        $besCal = computeBesCal($user['sexe'], $_POST['poids'], $user['taille'], $age, $_POST['nivActivite']);

        $resultats = getResultats($user['id']);
        if( $today > $resultats[0]['date'] ) {
            // Pas de données pour today. Créer de nouveaux Resultats.
            createResultat($user['id'], $today, $_POST['poids'], $imc, $_POST['nivActivite'], $besCal);
        } else {
            // Données existant déjà pour ajourd'hui. Resultats à update.
            updateResultat($resultats[0]['id'], $user['id'], $today, $_POST['poids'], $imc, $_POST['nivActivite'], $besCal);
        }
        header('Location: v_profils.php#Myl-Myd-1');
    }
}

?>