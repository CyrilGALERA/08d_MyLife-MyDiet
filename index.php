<?php
  session_start();
  $_SESSION['connectMsg'] = 'Veuillez vous connecter ou vous inscrire pour utiliser l appli My Life - My Diet';

  include 'a_config.php';
  include 'v0_header.php';
?>

  <main>
    <h2 class="padding-V5 center-txt">Accueil</h2>
    <div class="box1 col padding-H20">
      <h3 class="t4-cyan padding-V5">Bienvenue sur l appli My life - My diet</h3>
      <p>Avec cette super appli, fini l'effet yoyo !</p>
      <p>Calculez votre IMC, besoins caloriques, archivez vos résultats et vous verrez perdre les kilos petit à petit !</p>
      <p>PUB pub et re-pub ! </p>

      <h3 class="t4-cyan padding-V5">Fonctionnement</h3>
      <p> Cliquer sur le menu "Connexion", puis :</p>
      <ul class="padding-H20">
        <li> "Se connecter" </li>
        <li> ou "S'inscrire" pour créer votre nouveau compte utilisateur</li>
      </ul>
      <p> Et laissez vous guider!</p>
  </main>

  <?php include 'v0_footer.php'; ?>

</body>
</html>