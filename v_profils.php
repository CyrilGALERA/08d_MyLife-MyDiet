<?php
  session_start();
  // echo 'session = '.$_SESSION['email'];
  if(!$_SESSION['email']) { 
    // echo'<br> not session email';
    //header('Location: v_connexion.php'); 
  } else {
    // echo'<br> session email = '.$_SESSION['email'];
    global $toConnect;
    $toConnect = "Déconnexion";
  }
  
  // else : display Web Page -----------------------------------------   
  include './DB_SQL/m_data_func.php';
  include 'v0_header.php';
  // include 'c_profils.php';
?>

  <main>

    <h2 class="padding-V5 center-txt">Profil My Life, My Diet</h2>

    <!-- Sous menu ----------- -->
    <nav class="padding-V10">
      <ul class="Nav-lu ligne axe1-sp-around">
        <li class="Nav-li">
            <a class="Nav-btn-a" href="./v_profils.php#Cpt1">Compte</a>
            <ul>
              <li>               
                <a class="" href="./v_profils.php#Cpt1">Supprimer son compte</a>
              </li>
              <li>               
                <a class="" href="./v_profils.php#Cpt1">Modifer ses infos perso</a>
              </li>
            </ul>
        </li>

        <li class="Nav-li">
            <a class="Nav-btn-a" href="./v_profils.php#Myl-Myd-1">My life, My diet</a>
            <ul>
              <li>               
                <a class="" href="./v_profils.php#Myl-Myd-1">Calculer son IMC</a>
              </li>
              <li>               
                <a class="" href="./v_profils.php#Myl-Myd-1">Calculer ses besoins caloriques</a>
              </li>
              <li>               
                <a class="" href="./v_profils.php#Myl-Myd-1">Historique des résultats</a>
              </li>
            </ul>
        </li>

        <li class="Nav-li">
            <a class="Nav-btn-a" href="./v_profils.php#Conso1">Services & Conseils</a>
            <ul>
              <li>               
                <a class="" href="./v_profils.php#Conso1">Email et Tel</a>
              </li>
              <li>               
                <a class="" href="./v_profils.php#Conso1">Conseils, coaching, promotion</a>
              </li>
            </ul>
        </li>
      </ul>
    </nav> <!-- End: Sous menu --------------------------------------------- -->

    <?php $user = getUser($_SESSION['email']); ?>

    <h3 class="padding-V5" id="Cpt1">Compte</h3>
    <div class="box1 padding-H100">

      <!-- Box: Compte / Suppression --------------- -->
      <h4 class="padding-V5">Supprimer son compte </h4>
      <div class="box1">
        <form class="form-box" action="c_profils.php" method="post">
        <!-- <form class="form-box" action="v_profils.php" method="post"> -->
        <!-- onSubmit="return validate()"> -->
          <div class="form-data">
            <label for="pwd">Password</label>
            <input type="text" name="pwd" id="pwd-form" value="" placeholder="Rentrez votre mot de passe...">
          </div>
          <!-- Le pwd User BDD et pwd rentré dans le form est d'abord vérifer en js ONCLICK="checkPwd1() -->
          <input type="hidden" name="pwd-suppr" id="pwd-suppr" value=<?php echo $user['password']?> >
          <BUTTON id="suppr-choix1" TYPE="button" ONCLICK="checkPwdSuppr()">Supprimer</BUTTON>
          <!-- Si les pwd sont identiques et que le user confirme : delete user en php POST -->
          <input type="hidden" name="idUser" value=<?php echo $user['id']?> >
          <input id="suppr-choix2" class="form-btn" type="submit" name="Confirmer-suppr" value="Confirmer">
        </form>
      </div>

      <!-- Box: Compte / Suppression --------------- -->
      <h4 class="padding-V5">Modifier son mot de passe </h4>
      <div class="box1">
        <form class="form-box" action="c_profils.php" method="post">
          <div class="col">
            <div class="form-data">
              <label for="pwd">Password (ancien)</label>
              <input type="text" name="pwd21" id="pwd-form21" value="" placeholder="Mot de passe ancien">
            </div>
            <div class="form-data">
              <label for="pwd">Password (nouveau)</label>
              <input type="text" name="pwd22" id="pwd-form22" value="" placeholder="Nouveau mot de passe">
            </div>
          </div>
          <div class="ligne-btn">
            <!-- Le pwd User BDD et pwd rentré dans le form sont d'abord vérifer en js ONCLICK="checkPwd2() -->
            <input type="hidden" name="pwd-modif" id="pwd-modif" value=<?php echo $user['password']?> >
            <BUTTON id="modif-choix1" TYPE="button" ONCLICK="checkPwdModif()">Modifier</BUTTON>
            <!-- Si les pwd sont identiques et que le user confirme : update pwd en php POST -->
            <input type="hidden" name="idUser" value=<?php echo $user['id']?> >
            <input id="modif-choix2" class="form-btn" type="submit" name="Modif-pwd" value="Confirmer">
          </div>  
        </form>
      </div>

      <!-- Box: Compte / Info perso --------------- -->
      <h4 class="padding-V5">Informations personnelles</h4>
      <div class="box1">

        <form class="form-box" action="c_profils.php" method="post">
          <div class="form-data">
            <label for="email">Email</label>
            <input type="text" name="email" value=<?php echo $user['email'];?> >
          </div>
          <input type="hidden" name="idUser" value=<?php echo $user['id']?> >
          <input class="form-btn" type="submit" name="Modif-email" value="Modifier">
        </form>

        <form class="form-box" action="c_profils.php" method="post">
          <div class="form-data">
            <label for="prenom">Prénom</label>
            <input type="text" name="prenom" value=<?php echo $user['prenom'];?> >
          </div>
          <input type="hidden" name="idUser" value=<?php echo $user['id']?> >
          <input class="form-btn" type="submit" name="Modif-prenom" value="Modifier">
        </form>

        <form class="form-box" action="c_profils.php" method="post">
          <div class="form-data">
            <label for="nom">Nom</label>
            <input type="text" name="nom" value=<?php echo $user['nom'];?> >
          </div>
          <input type="hidden" name="idUser" value=<?php echo $user['id']?> >
          <input class="form-btn" type="submit" name="Modif-nom" value="Modifier">
        </form>

        <form class="form-box" action="c_profils.php" method="post">
          <div class="form-data">
            <label for="dateAnniv">Date de naissance</label>
            <input type="date" name="dateAnniv" value=<?php echo $user['dateNaissance'];?> >
          </div>
          <input type="hidden" name="idUser" value=<?php echo $user['id']?> >
          <input class="form-btn" type="submit" name="Modif-dateAnniv" value="Modifier">
        </form>

        <!-- <?php echo 'sexe = '.$user['sexe'];?> -->
        <form class="form-box" action="c_profils.php" method="post">
        <!-- <form class="form-box" method="post"> -->
          <div class="form-data">
            <label for="sexe">Sexe :</label>
            <select name="sexe">

                <option value="">Sexe</option>
                <?php if ($user['sexe'] == 'homme') { ?>
                  <option value='homme' selected>Homme</option>
                  <option value='femme'>Femme</option>
                <?php } else if ($user['sexe'] == 'femme') { ?>
                  <option value='homme'>Homme</option>
                  <option value='femme' selected>Femme</option>
                <?php } else { ?>
                  <option value='homme'>Homme</option>
                  <option value='femme'>Femme</option>
                <?php } ?>

            </select>
          </div>
          <input type="hidden" name="idUser" value=<?php echo $user['id']?> >
          <input class="form-btn" type="submit" name="Modif-sexe" value="Modifier">
        </form>

        <form class="form-box" action="c_profils.php" method="post">
        <!-- <form class="form-box" method="post"> -->
          <div class="form-data">
            <label for="taille">Taille</label>
            <input type="number" step=0.01 name="taille" value=<?php echo $user['taille'];?> >
          </div>
          <input type="hidden" name="idUser" value=<?php echo $user['id']?> >
          <input class="form-btn" type="submit" name="Modif-taille" value="Modifier">
        </form>

      </div> <!-- End: Box 1.2 Info perso  -->
    </div> <!-- End: Box 1 Compte ------------------------------------------------- -->

    <!-- Box: My life My diet   --------------------------------------------------- -->
    <?php $resultats = getResultats($user['id']); 
      if( !$resultats ) {
        $resultats[0] = array( 
          'id' => NULL, 'idUser' => NULL, 'date' =>NULL, 'poids' =>NULL, 
          'imc'=>NULL, 'nivActivite'=>NULL, 'besoinsCal'=>NULL,
        );
    } ?>

    <div class="ligne axe1-sp-between">
      <h3 class="padding-V5" id="Myl-Myd-1">My Life, My Diet</h3>
      <a class="btn-top" href="#top-banniere">&uArr;</a>        
    </div>
    <div class="box1 col">

      <h4 class="padding-V5">Calculer son IMC & ses besoins caloriques</h4>
      <div class="box1 col">

        <form class="form-box-col padding-H100" action="c_profils.php" method="post">
          <div class="ligne axe1-sp-between">
            <label for="poids">Poids</label>
            <input type="number" step=0.1 name="poids" value=<?php echo $resultats[0]['poids'];?> >
          </div>
          <div class="ligne axe1-sp-between">
            <label for="nivActivite">Niveau d'activité :</label>
            <input type="number" step=0.005 name="nivActivite" value=<?php echo $resultats[0]['nivActivite'];?> >
          </div>
          <input type="hidden" name="idUser" value=<?php echo $user['id']?> >
          <input class="form-btn" type="submit" name="calculer" value="Calculer">
        </form>

        <div class="box2-blanc">
          <div class="ligne axe1-sp-between padding-H10">
            <p>RESULTAT : </p>
            <p>Date : <?php echo date('Y-m-d') ?> </p>
          </div>
          <ul class="padding-H50">
            <li class="bg-blanc"> <?php echo printTodayResultats($resultats)[0] ?></li>
            <li class="bg-blanc"> <?php echo printTodayResultats($resultats)[1] ?></li>
          </ul>
        </div>

        <div class="scroll-bar">
        <div class="padding-V5">
          <p class="padding-H10">IMC : </p>
          <ul class="padding-H50">
            <li> En dessous de 18,4 kg/m² &nbsp;: on considère que la personne est maigre, </li>
            <li> Entre 18,5 &nbsp;et &nbsp;&nbsp;24,9 kg/m²&nbsp;&nbsp;: on considère que la personne a une corpulence "normale", </li>
            <li> Entre 25 &nbsp;&nbsp;&nbsp;&nbsp;et &nbsp;&nbsp;29,9 kg/m²  &nbsp;: on considère que la personne est en surpoids, </li>
            <li> Entre 30 &nbsp;&nbsp;&nbsp;&nbsp;à &nbsp;&nbsp;&nbsp;34,9 kg/m² &nbsp;: on considère que la personne est en obésité modérée, </li>
            <li> Entre 35 &nbsp;&nbsp;&nbsp;&nbsp;et &nbsp;&nbsp;39,9 kg/m²  &nbsp;: on considère que la personne est en obésité sévère, </li>
            <li> Au-dessus de &nbsp;&nbsp;40 &nbsp;&nbsp;kg/m² &nbsp; : on considère que la personne est en obésité morbide. </li>
          </ul>
        </div>

        <div class="padding-V5">
          <p class="padding-H10">NIVEAU D'ACTIVITE : </p>
          <ul class="padding-H50">
            <li>1,2&nbsp;&nbsp;&nbsp;&nbsp; : vous avez un travail de bureau ou une faible dépense sportive (profil sédentaire) </li>
            <li>1,375 : vous vous entraînez 1 à 3 fois par semaine (profil profil légèrement actif)</li>
            <li>1,55&nbsp;&nbsp; : vous vous entraînez 4 à 6 fois par semaine (profil actif)</li>
            <li>1,725 : vous faites quotidiennement du sport ou des exercices physiques très soutenus (profil très actif)</li>
          <ul>
        </div>
        </div>
      </div> <!-- End: Box 2.2 Calculer son IMC et ses Besoins Caloriques -->

      <!-- Box 2.3 Historique des résultats -->
      <h4 class="padding-V5">Historique des résultats</h4>
      <div class="box1-resultat scroll-bar">
        <div class="min-width">
        <table style="width:100%">
          <thead> 
            <tr> 
              <th>Date</th>
              <th>Poids</th>
              <th>Corpulence</th>
              <th>IMC</th>
              <th>Niv. Activité</th>
              <th>Bes. Calorique</th>
            </tr> 
          </thead>
          <tbody> 
            <?php foreach($resultats as $res) { ?>
              <tr>  
                <td><?php echo $res['date'];?> </td>
                <td><?php echo $res['poids'];?> </td>
                <td><?php echo corpulence($res['imc']);?> </td>
                <td><?php echo $res['imc'];?> </td>
                <td><?php echo $res['nivActivite'].' &nbsp; &nbsp; '.profilActivite($res['nivActivite']) ?> </td>
                <td><?php echo $res['besoinsCal'];?> </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
        </div>

      </div> <!-- End: Box 2.3 Historique des résultats  -->


    </div> <!-- End: Box 2 My life - My diet ------------------------------------------------- -->

    <!-- Box: Services & Conseils --------------- -->
    <div class="ligne axe1-sp-between">
      <h3 class="padding-V5" id="Conso1">Services & Conseils</h3>
      <a class="btn-top" href="#top-banniere">&uArr;</a>        
    </div>
    <div class="box1 col">

      <h4 class="padding-V5">Contacter le service d'accompagnement</h4>
      <div class="ligne axe1-sp-around">
        <p>Email : suivi.conso@myl-myd.com</p>
        <p>Tel : 06 03 66 27 41</p>
      </div>

      <div class="box1 padding-H20 col">
        <ul>
          <li><p>Profitez de nos conseils, coach diet, coach sportif, nutritioniste…</p></li>
          <li><p>Prix adapté à tous les budgets et tout type de suivi (journalier, hebdomadaire ou mensuel).</p></li>
          <li><p>Profitez de nos réduction avec nos partenaires (centres de fitness...) dans votre région !</p></li>
        </ul>
      </div> <!-- End: Box 3 Service (texte) -->
    </div> <!-- End: Box 3 Services & Conseils (box principale)-------------------------------- -->

  </main>
  <?php include 'v0_footer.php'; ?>
  <script src="./JS/script-profils.js"></script>
</body>
</html>