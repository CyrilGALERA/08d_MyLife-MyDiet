
-- -------------------------------------------------------------
-- DATABASE:    MyLife_MyDiet;

-- TYPE ENUM:   EtatCommande
-- TABLE:       Users, IMC, BesoinsCaloriques
--
-- -------------------------------------------------------------
-- Use script: sudo mysql < DB_init.sql;
--
-- sudo mysql
-- mysql> ...
-- show databases;
-- use MyLife_MyDiet;
-- show tables;
-- describe (table_name);
-- select * from (table_name);
-- quit;
-- exit;
-- -------------------------------------------------------------

DROP DATABASE IF EXISTS MyLife_MyDiet;

CREATE DATABASE MyLife_MyDiet;
-- CREATE USER 'u_cyril'@'localhost' IDENTIFIED BY 'pwd_cyril';
-- GRANT ALL PRIVILEGES ON *.* TO 'u_cyril'@'localhost';
GRANT ALL PRIVILEGES ON MyLife_MyDiet.* TO 'u_cyril'@'localhost';

USE MyLife_MyDiet;

SELECT "DATABASE MyLife_MyDiet created for user u_cyril";

-- NEW TABLE -----------------------------
CREATE TABLE Users (
    id              INT unsigned auto_increment PRIMARY KEY,
    email           varchar(50),
    password        varchar(25),
    nom             varchar(25),
    prenom          varchar(25),
    dateNaissance   date,
    sexe            ENUM('homme','femme'),
    taille          FLOAT,
    UNIQUE(email)
);

CREATE TABLE Resultats (
    id              INT unsigned auto_increment PRIMARY KEY,
    idUser          INT unsigned,
    date            date,
    poids           FLOAT,
    imc             FLOAT,
    nivActivite     FLOAT,
    besoinsCal      FLOAT,
    FOREIGN KEY (idUser) REFERENCES Users (id) ON DELETE CASCADE
);



