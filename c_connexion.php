<?php
    // // si c_connexion.php n est pas include dans v_connexion.php
    // // s'il est indépendant de v_connexion.php
    // session_start();
    // include './DB_SQL/m_data_func.php';

    // si v_connexion.php fait un include de c_connexion.php
    // pas la peine de redeclarer :
    //    session_start();
    //    include './DB_SQL/m_data_func.php';

//Si l'utilsateur clique sur le bouton SeConnecter 
if(isset($_POST['SeConnecter'])){

    // echo "<br>";
    // echo "c_connexion.php";

    // Je déclare mes variables 
    global $pdo;
    global $toConnect;
    $email = $_POST['email'];
    $pwd = $_POST['pwd'];
   
    // Je recupere sous forme de tableau,les mots de passe et les login dans la table admin de ma base de donnée
    $sql = 'SELECT * FROM Users WHERE email = ? AND password = ?';
    $users = $pdo->prepare($sql);

    // Je Vérifie s'il trouve un user avec le $login $pwd du form POST
    $result = $users->execute(array($email, $pwd));
    $user_found = $users->rowCount();

    if($user_found > 0){
        global $pdo;
        $_SESSION['email'] = $email;
        $_SESSION['password'] = $pwd;
        $user = getUser($email);

        // echo "  .......... $_SESSION[email] = " . $_SESSION['email'] ."<br>"; 
        // echo "  .......... $_SESSION[password] = " . $_SESSION['password'] ."<br>"; 
        // echo "  .......... $_SESSION[id] = " . $_SESSION['id'] ."<br>"; 
        // echo "user2 = ".$user['prenom']."<br>";
        // var_dump($user);
        
        global $toConnect;
        $toConnect = "Connecté";
        $connectionResult = 'Etes vous sûr de vouloir vous déconnecter ?';
        header('Location: v_profils.php');
    } else {
        // Si les identifiants sont incorrect : l'utilisateur est renvoyé vers index.html via la balise meta HTML   
        $_SESSION['password'] = NULL;
        $_SESSION['email'] = NULL;
        $_SESSION['connectMsg'] = 'Vos identifiants sont incorrects... Veuillez recommencez';
        // global $toConnect;
        // $toConnect = NULL;
        $connectionResult = 'Vos identifiants sont incorrects... Veuillez recommencez';
        header('Location: v_connexion.php');
    }
}

if(isset($_POST['S-inscrire']) ) {
    if( isset($_POST['email']) && $_POST['email'] != ""
        && isset($_POST['pwd']) && $_POST['pwd'] != ""
        && isset($_POST['prenom']) && $_POST['prenom'] != ""
        && isset($_POST['nom']) && $_POST['nom'] != ""
        && isset($_POST['dateAnniv']) && $_POST['dateAnniv'] != ""  
    ) {
        $email = $_POST['email'];
        $pwd = $_POST['pwd'];

        if (getUser($email)) {
            global $toConnect;
            $toConnect = NULL;
            $_SESSION['connectMsg'] = 'Un compte existe déjà avec cet email';
            // echo '<br> connectionResult = '.$connectionResult.'<br>';
            header('Location: v_connexion.php');
        } else {
            $_SESSION['email'] = $email;
            $_SESSION['password'] = $pwd;
            // $_SESSION['suppr-cpt'] = NULL;

            createUser($email, $pwd, $_POST['nom'], $_POST['prenom'], $_POST['dateAnniv']);
            // $user = getUser($email);
            // echo '<br> User = <br>';
            // var_dump($user);
            global $toConnect;
            $toConnect = "Déconnexion";
            $connectionResult = 'Etes vous sûr de vouloir vous déconnecter ?';
            // $_SESSION['connectMsg'] = 'Etes vous sûr de vouloir vous déconnecter ?'; // ????
            // echo '<br> connectionResult = '.$connectionResult.'<br>';
            header('Location: v_profils.php');
        }
    } else {
            // global $toConnect;
            // $toConnect = NULL;
            $_SESSION['connectMsg'] = 'Certains champs ne sont pas ou sont mal renseignés';
            // echo '<br> connectionResult = '.$connectionResult.'<br>';
            header('Location: v_connexion.php');
    }
}


if (isset($_POST['SeDeconnecter']) ) {
    global $toConnect;
    $toConnect = NULL;
    $_SESSION['email'] = NULL;
    $_SESSION['password'] = NULL;
    // $_SESSION['suppr-cpt'] = NULL;
    session_destroy();
    header('Location: v_connexion.php');
}

?>