-- Use script: sudo mysql < DB_fill.sql;

-- DATABASE:    MyLife_MyDiet;
-- TABLE:       Users, IMC, BesoinsCaloriques
--
-- -------------------------------------------------------------
-- Use script: sudo mysql < DB_init.sql;
--
-- sudo mysql
-- mysql> ...
-- show databases;
-- use MyLife_MyDiet;
-- show tables;
-- describe (table_name);
-- select * from (table_name);
-- quit;
-- exit;
-- -------------------------------------------------------------

USE MyLife_MyDiet;

-- TABLE Users, IMC, BesoinsCaloriques -----------------------------

INSERT INTO Users (email, password, nom, prenom, dateNaissance, sexe, taille)
  VALUES ("cyril", "cyril123", "Galera",      "Cyril",  "1975-12-18", 'homme', 1.75),
         ("kevin", "kevin123", "Simplon",     "Kevin",  "2000-01-01", 'homme', 1.78),
         ("bob",   "bob123",   "Sinclair",    "Bob",    "1980-08-25", 'homme', 1.76),
         ("Marie", "mama123",  "De Narbonne", "Marie",  "1985-08-24", 'femme', 1.60);

INSERT INTO Resultats (idUser, date, poids, imc, nivActivite, besoinsCal)
  VALUES (1, '2022-08-25', 79,   25.8, 1.2, 1800),
         (1, "2022-08-24", 79.5, 26,   1.2, 1800),
         (1, "2022-08-23", 80,   26.1, 1.2, 1800),
         (1, "2022-08-15", 78, 25.5, 1.4, 2100),
         (1, "2022-08-10", 77, 25.1, 1.5, 2250),
         (1, "2022-08-01", 76, 24.8, 1.5, 2100),
         (1, "2022-07-01", 75, 24.5, 1.25, 1800);

         