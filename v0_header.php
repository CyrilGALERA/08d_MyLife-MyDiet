<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="icon" type="image/png" sizes="16x16" href="./IMAGES/favicon-MylMyd.png">
  <link rel="stylesheet" href="./CSS/styleU.css">
  <link rel="stylesheet" href="./CSS/style.css">
  <title>My Life, My Diet</title>
</head>

<body>
    <header class="margin-V10 margin-H10">
        <!-- <div class="ligne axe1-sp-around axe2-center"> -->
        <!-- <div id="top-banniere" class="col axe1-center axe2-center margin-H10"> -->
        <div class="col axe1-center axe2-center margin-H10">
            <div id="top-banniere" class="col axe1-center axe2-center">
                <h1 class="center-txt margin-V10"> My Life - My Diet </h1>
            </div>
        </div>

        <nav class="padding-V10">
            <ul class="Nav-lu ligne axe1-sp-around">
                <li class="Nav-btn-li">
                    <a class="Nav-btn-a" href="./index.php">Accueil</a>
                </li>

                <li class="Nav-btn-li">
                    <?php 
                    global $toConnect;
                    // var_dump($toConnect);
                    if( $toConnect != NULL ) { ?>
                        <a class="Nav-btn-a" href="./v_deconnexion.php">Déconnexion</a>
                    <?php } else { ?> 
                        <a class="Nav-btn-a" href="./v_connexion.php">Connexion</a>
                    <?php } ?> 
                </li>

                <li class="Nav-btn-li">
                    <a class="Nav-btn-a" href="./v_profils.php">Profil</a>
                </li>
            </ul>
        </nav>
        
    </header>
